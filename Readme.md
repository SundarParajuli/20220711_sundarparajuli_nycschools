# NYCHighSchool Application
## Architecture
1. Dependency Injection using Dagger Hilt
2. Network call using retrofit
3. Coroutines
4. MVVM pattern
5. Data binding and View binding
6. Jetpack Navigation
7. Unit testing of ViewModels
