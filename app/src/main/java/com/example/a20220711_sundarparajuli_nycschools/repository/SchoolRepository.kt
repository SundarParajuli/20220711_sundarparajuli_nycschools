package com.example.a20220711_sundarparajuli_nycschools.repository

import com.example.a20220711_sundarparajuli_nycschools.api.NycApi
import com.example.a20220711_sundarparajuli_nycschools.data.School
import com.example.a20220711_sundarparajuli_nycschools.data.SchoolSAT
import retrofit2.Response
import javax.inject.Inject

interface SchoolRepository {
    suspend fun getSchoolsList(): Response<List<School>>

    suspend fun getSchoolsSAT(): Response<List<SchoolSAT>>
}
class SchoolRepositoryImpl @Inject constructor(private val nycApi: NycApi) :
    SchoolRepository {

    override suspend fun getSchoolsList(): Response<List<School>> = nycApi.getSchoolsList()

    override suspend fun getSchoolsSAT(): Response<List<SchoolSAT>> = nycApi.getSchoolsSAT()
}