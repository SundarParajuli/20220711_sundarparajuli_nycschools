package com.example.a20220711_sundarparajuli_nycschools.api

import com.example.a20220711_sundarparajuli_nycschools.data.School
import com.example.a20220711_sundarparajuli_nycschools.data.SchoolSAT
import retrofit2.Response
import javax.inject.Inject

interface NycApi {
    suspend fun getSchoolsList(): Response<List<School>>
    suspend fun getSchoolsSAT(): Response<List<SchoolSAT>>

}
class NycApiImpl @Inject constructor(private val apiService: ApiService) : NycApi {
    override suspend fun getSchoolsList(): Response<List<School>> {
        return apiService.getSchoolsList()
    }

    override suspend fun getSchoolsSAT(): Response<List<SchoolSAT>> {
        return apiService.getSchoolsSAT()
    }
}