package com.example.a20220711_sundarparajuli_nycschools.ui.schoolList.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220711_sundarparajuli_nycschools.data.School
import com.example.a20220711_sundarparajuli_nycschools.repository.SchoolRepository
import com.example.a20220711_sundarparajuli_nycschools.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.net.UnknownHostException
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(private val schoolRepository: SchoolRepository) :
    ViewModel() {

    private val _schools = MutableLiveData<Resource<List<School>>>()

    val schoolsList: LiveData<Resource<List<School>>>
        get() = _schools

    init {
        getSchools()
    }

    fun getSchools() {
        _schools.value = Resource.loading()

        viewModelScope.launch {
            try {
                val resource = schoolRepository.getSchoolsList()
                if (resource.isSuccessful) {
                    _schools.value = Resource.success(resource.body() ?: listOf())
                } else {
                    _schools.value = Resource.error(resource.message() ?: "Something went wrong")
                }
            } catch (e: Exception) {
                if (e is UnknownHostException) {
                    _schools.value = Resource.error("No internet connection")
                } else {
                    _schools.value = Resource.error(e.localizedMessage ?: "Something went wrong")
                }
            }
        }
    }
}