package com.example.a20220711_sundarparajuli_nycschools.repository

import com.example.a20220711_sundarparajuli_nycschools.data.School
import com.example.a20220711_sundarparajuli_nycschools.data.SchoolSAT
import retrofit2.Response

class FakeSchoolRepository :
    SchoolRepository {
    private val fakeSchoolList = listOf(
        School("32", "test1", "a@abc.com", "", ""),
        School("42", "test2", "a@def.com", "", "")
    )

    private val fakeSchoolSATList = listOf(
        SchoolSAT("103", "12", "15", "122", "141", "157")
    )

    override suspend fun getSchoolsList(): Response<List<School>> {
        return Response.success(fakeSchoolList)
    }

    override suspend fun getSchoolsSAT(): Response<List<SchoolSAT>> {
        return Response.success(fakeSchoolSATList)

    }
}